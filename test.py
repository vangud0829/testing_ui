import time
from selenium import webdriver

driver = webdriver.chrome()

driver.get("http://suninjuly.github.io/simple_form_find_task.html")

start = time.time()
textarea1 = driver.find_element_by_css_selector(".form-group:first-child>input")
textarea1.send_keys("Иван")

textarea2 = driver.find_element_by_css_selector(".form-group:nth-child(2)>input")
textarea2.send_keys("Не Иван")

textarea3 = driver.find_element_by_css_selector(".form-group:nth-child(3)>input")
textarea3.send_keys("Москва")

textarea4 = driver.find_element_by_css_selector(".form-group:nth-child(4)>input")
textarea4.send_keys("Не Москва")

submit_button = driver.find_element_by_css_selector("#submit_button")

submit_button.click()

print("Время выполнения:", 	time.time() - start)

driver.quit()
